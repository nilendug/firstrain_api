package com.firstrain.frapi.domain;

import java.util.Date;

public class ManagementChange {
	private String newCompany;
	private String oldCompany;
	private String newDesignation;
	private String oldDesignation;
	private String newTicker;
	private String oldTicker;
	private int newCompanyId;
	private int oldCompanyId;
	private String person;
	private String changeType;
	private String oldRegion;
	private String newRegion;
	private String oldGroup;
	private String newGroup;
	private Date date;
	private boolean linkable;
	private String url;
	private boolean futureEvent = false;
	private long trendId;

	public enum MgmtChangeType {
		HIRE, DEPARTURE, INTERNALMOVE
	}

	public String getNewCompany() {
		return newCompany;
	}

	public void setNewCompany(String newCompany) {
		this.newCompany = newCompany;
	}

	public String getOldCompany() {
		return oldCompany;
	}

	public void setOldCompany(String oldCompany) {
		this.oldCompany = oldCompany;
	}

	public String getNewDesignation() {
		return newDesignation;
	}

	public void setNewDesignation(String newDesignation) {
		this.newDesignation = newDesignation;
	}

	public String getOldDesignation() {
		return oldDesignation;
	}

	public void setOldDesignation(String oldDesignation) {
		this.oldDesignation = oldDesignation;
	}

	public String getNewTicker() {
		return newTicker;
	}

	public void setNewTicker(String newTicker) {
		this.newTicker = newTicker;
	}

	public String getOldTicker() {
		return oldTicker;
	}

	public void setOldTicker(String oldTicker) {
		this.oldTicker = oldTicker;
	}

	public int getNewCompanyId() {
		return newCompanyId;
	}

	public void setNewCompanyId(int newCompanyId) {
		this.newCompanyId = newCompanyId;
	}

	public int getOldCompanyId() {
		return oldCompanyId;
	}

	public void setOldCompanyId(int oldCompanyId) {
		this.oldCompanyId = oldCompanyId;
	}

	public String getPerson() {
		return person;
	}

	public void setPerson(String person) {
		this.person = person;
	}

	public String getChangeType() {
		return changeType;
	}

	public void setChangeType(String changeType) {
		this.changeType = changeType;
	}

	public String getOldRegion() {
		return oldRegion;
	}

	public void setOldRegion(String oldRegion) {
		this.oldRegion = oldRegion;
	}

	public String getNewRegion() {
		return newRegion;
	}

	public void setNewRegion(String newRegion) {
		this.newRegion = newRegion;
	}

	public String getOldGroup() {
		return oldGroup;
	}

	public void setOldGroup(String oldGroup) {
		this.oldGroup = oldGroup;
	}

	public String getNewGroup() {
		return newGroup;
	}

	public void setNewGroup(String newGroup) {
		this.newGroup = newGroup;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public boolean isLinkable() {
		return linkable;
	}

	public void setLinkable(boolean linkable) {
		this.linkable = linkable;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public boolean isFutureEvent() {
		return futureEvent;
	}

	public void setFutureEvent(boolean futureEvent) {
		this.futureEvent = futureEvent;
	}

	public long getTrendId() {
		return trendId;
	}

	public void setTrendId(long trendId) {
		this.trendId = trendId;
	}
}
