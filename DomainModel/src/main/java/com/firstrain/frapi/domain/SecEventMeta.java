package com.firstrain.frapi.domain;

import org.apache.log4j.Logger;
import org.codehaus.jackson.annotate.JsonIgnore;

import com.firstrain.frapi.util.DefaultEnums.EventTypeEnum;
import com.firstrain.utils.FR_Encoder;

public class SecEventMeta extends MetaEvent {
	private static final Logger LOG = Logger.getLogger(SecEventMeta.class);

	private String secId;
	private String originalTitle;
	private String secFormType;

	public String getSecId() {
		return secId;
	}

	public void setSecId(String secId) {
		this.secId = secId;
	}

	public String getOriginalTitle() {
		return originalTitle;
	}

	public void setOriginalTitle(String originalTitle) {
		this.originalTitle = originalTitle;
	}

	public String getSecFormType() {
		return secFormType;
	}

	public void setSecFormType(String secFormType) {
		this.secFormType = secFormType;
	}

	@JsonIgnore
	public String getSeceTitle(String eventType) {
		String secFormType;
		if (("TYPE_TEN_K_EVENT").equals(eventType)) { // int TYPE_TEN_K_FILING = 450;
			if (originalTitle.contains("10-K/A")) {
				secFormType = "10-K/A";
			} else {
				secFormType = "10-K";
			}
		} else if (("TYPE_TEN_Q_EVENT").equals(eventType)) { // int TYPE_TEN_Q_FILING = 460;
			if (originalTitle.contains("10-Q/A")) {
				secFormType = "10-Q/A";
			} else {
				secFormType = "10-Q";
			}
		} else if (("TYPE_DELAYED_SEC_FILING").equals(eventType)) { // int TYPE_DELAYED_SEC_FILING = 500;
			return getSecDelayedTitle("", originalTitle, "SEC Delay", true); // SEC Delay<==>EventGroupEnum.GROUP_DELAYED_FILING.getLabel()
		} else {
			if (originalTitle.contains("8-K/A")) {
				secFormType = "8-K/A";
			} else {
				secFormType = "8-K";
			}
			EventTypeEnum e = EventTypeEnum.valueOf(EventTypeEnum.class, eventType);
			if (e != null) {
				originalTitle = e.getLabel().replaceFirst("Filed an 8K Statement on ", "");
			}
		}
		String construtedTitle = getSecTitle(originalTitle, secFormType);

		return construtedTitle;
	}

	@JsonIgnore
	public String getSeceTitleWithoutHtml(String eventType) {
		String secFormType;
		if (("TYPE_TEN_K_EVENT").equals(eventType)) { // int TYPE_TEN_K_FILING = 450;
			if (originalTitle.contains("10-K/A")) {
				secFormType = "10-K/A";
			} else {
				secFormType = "10-K";
			}
		} else if (("TYPE_TEN_Q_EVENT").equals(eventType)) { // int TYPE_TEN_Q_FILING = 460;
			if (originalTitle.contains("10-Q/A")) {
				secFormType = "10-Q/A";
			} else {
				secFormType = "10-Q";
			}
		} else if (("TYPE_DELAYED_SEC_FILING").equals(eventType)) { // int TYPE_DELAYED_SEC_FILING = 500;
			return getSecDelayedTitleWithoutHtml("", originalTitle, "Delayed SEC Filings"); // Delayed SEC Filings <==>
																							// EventGroupEnum.GROUP_DELAYED_FILING.getLabel());
		} else {
			if (originalTitle.contains("8-K/A")) {
				secFormType = "8-K/A";
			} else {
				secFormType = "8-K";
			}
			EventTypeEnum e = EventTypeEnum.valueOf(EventTypeEnum.class, eventType);
			if (e != null) {
				originalTitle = e.getLabel().replaceFirst("Filed an 8K Statement on ", "");
			}
		}
		String construtedTitle = getSecTitleWithoutHtml(originalTitle, secFormType);

		return construtedTitle;
	}

	@JsonIgnore
	public String getSecTitle() {
		return getSecTitle(originalTitle, secFormType);
	}

	private String getSecTitle(String originalTitle, String secFormType) {
		String constructedTitle = "";
		int index = originalTitle.indexOf(":");
		if (index != -1) {
			originalTitle = originalTitle.substring(index + 1);
		}
		String[] subTitles = originalTitle.split(","); // we have company name before :
		String st = null;
		if (subTitles.length > 1) {
			st = subTitles[1].trim();
		}
		if ("10-K".equals(secFormType)) {
			// Filed an Annual Report for FY <XXXX>
			constructedTitle += String.format("Filed an " + ("<strong>") + "Annual Report</strong> for FY %s", st.replaceFirst("FY", ""));
		} else if ("10-K/A".equals(secFormType)) {
			// Filed an Annual Report for FY <XXXX>
			constructedTitle +=
					String.format("Amended the " + ("<strong>") + "Annual Report</strong> for FY %s", st.replaceFirst("FY", ""));
		} else if ("10-Q".equals(secFormType)) {
			// Filed a Quarterly Report for <YY>, FY <XXXX>
			constructedTitle += String.format("Filed a " + ("<strong>") + "Quarterly Report</strong> for %s, FY %s", st,
					subTitles[2].trim().replaceFirst("FY", ""));
		} else if ("10-Q/A".equals(secFormType)) {
			// Filed a Quarterly Report for <YY>, FY <XXXX>
			constructedTitle += String.format("Amended the " + ("<strong>") + "Quarterly Report</strong> for %s, FY %s", st,
					subTitles[2].trim().replaceFirst("FY", ""));
		} else if ("8-K".equals(secFormType)) {
			// Filed an 8K Statement on <ABC>.
			if (st != null) {
				constructedTitle += String.format("Filed an " + ("<strong>") + "8K Statement</strong> on %s, %s",
						subTitles[0].trim().replaceFirst("8-K ", "").replaceFirst("- ", ""), st);
			} else {
				constructedTitle += String.format("Filed an " + ("<strong>") + "8K Statement</strong> on %s",
						subTitles[0].trim().replaceFirst("8-K ", "").replaceFirst("- ", ""));
			}
		} else if ("8-K/A".equals(secFormType)) {
			// Filed an 8K Statement on <ABC>.
			if (st != null) {
				constructedTitle += String.format("Amended the " + ("<strong>") + "8K Statement</strong> on %s, %s",
						subTitles[0].trim().replaceFirst("8-K ", "").replaceFirst("- ", ""), st);
			} else {
				constructedTitle += String.format("Amended the " + ("<strong>") + "8K Statement</strong> on %s",
						subTitles[0].trim().replaceFirst("8-K ", "").replaceFirst("- ", ""));
			}
		}
		return constructedTitle;
	}

	@JsonIgnore
	public String getSecDelayedTitle(String companyName, String title, String prefIx, boolean inLineStyle) {
		String constructedTitle = "";
		if (companyName != null) {
			companyName = FR_Encoder.encodeXML(companyName, FR_Encoder.CHARACTER_ENCODING);
			constructedTitle += inLineStyle ? "<span>" + companyName + "</span> <br/>" : "<span>" + companyName + "</span> <br/>";
		}
		constructedTitle += prefIx + ": " + title;
		return constructedTitle;
	}

	@JsonIgnore
	public String getSecDelayedTitleWithoutHtml(String companyName, String title, String prefIx) {
		String constructedTitle = "";
		if (companyName != null) {
			companyName = FR_Encoder.encodeXML(companyName, FR_Encoder.CHARACTER_ENCODING);
			constructedTitle += companyName;
		}
		constructedTitle += prefIx + " : " + title;
		return constructedTitle;
	}

	@JsonIgnore
	public String getSecTitleWithoutHtml() {
		return getSecTitleWithoutHtml(originalTitle, secFormType);
	}

	private String getSecTitleWithoutHtml(String title, String secFormType) {
		String orgTitle = title;
		String constructedTitle = "";
		try {
			int index = title.indexOf(":");
			if (index != -1) {
				title = title.substring(index + 1);
			}
			String[] subTitles = title.split(","); // we have company name before :
			String st = null;
			if (subTitles != null && subTitles.length > 1) {
				st = subTitles[1].trim();
			}
			if ("10-K".equals(secFormType)) {
				// Filed an Annual Report for FY <XXXX>
				constructedTitle += String.format("Filed an Annual Report for FY %s", st.replaceFirst("FY", ""));
			} else if ("10-K/A".equals(secFormType)) {
				// Filed an Annual Report for FY <XXXX>
				constructedTitle += String.format("Amended the Annual Report for FY %s", st.replaceFirst("FY", ""));
			} else if ("10-Q".equals(secFormType)) {
				// Filed a Quarterly Report for <YY>, FY <XXXX>
				constructedTitle += String.format("Filed a Quarterly Report for %s, FY %s", st, subTitles[2].trim().replaceFirst("FY", ""));
			} else if ("10-Q/A".equals(secFormType)) {
				// Filed a Quarterly Report for <YY>, FY <XXXX>
				constructedTitle +=
						String.format("Amended the Quarterly Report for %s, FY %s", st, subTitles[2].trim().replaceFirst("FY", ""));
			} else if ("8-K".equals(secFormType)) {
				// Filed an 8K Statement on <ABC>.
				if (st != null) {
					constructedTitle += String.format("Filed an 8K Statement on %s, %s",
							subTitles[0].trim().replaceFirst("8-K ", "").replaceFirst("- ", ""), st);
				} else {
					constructedTitle += String.format("Filed an 8K Statement on %s",
							subTitles[0].trim().replaceFirst("8-K ", "").replaceFirst("- ", ""));
				}
			} else if ("8-K/A".equals(secFormType)) {
				// Filed an 8K Statement on <ABC>.
				if (st != null) {
					constructedTitle += String.format("Amended the 8K Statement on %s, %s",
							subTitles[0].trim().replaceFirst("8-K ", "").replaceFirst("- ", ""), st);
				} else {
					constructedTitle += String.format("Amended the 8K Statement on %s",
							subTitles[0].trim().replaceFirst("8-K ", "").replaceFirst("- ", ""));
				}
			}
		} catch (Exception e) {
			LOG.error("Error for title: " + orgTitle, e);
			return orgTitle;
		}
		return constructedTitle;
	}
}
