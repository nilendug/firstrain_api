package com.firstrain.web.pojo;

import java.io.Serializable;
import java.util.List;

import org.codehaus.jackson.annotate.JsonPropertyOrder;

import com.firstrain.web.pojo.Document;

@JsonPropertyOrder({"searchId", "searchName", "searchQuery", "searchFilter", "documents"})
public class Search implements Serializable {

	private static final long serialVersionUID = -3345179857673122369L;
	private String searchId;
	private String searchName;
	private String searchQuery;
	private String searchFilter;
	private List<Document> documents;

	public String getSearchId() {
		return searchId;
	}

	public void setSearchId(String searchId) {
		this.searchId = searchId;
	}

	public String getSearchName() {
		return searchName;
	}

	public void setSearchName(String searchName) {
		this.searchName = searchName;
	}

	public String getSearchQuery() {
		return searchQuery;
	}

	public void setSearchQuery(String searchQuery) {
		this.searchQuery = searchQuery;
	}

	public String getSearchFilter() {
		return searchFilter;
	}

	public void setSearchFilter(String searchFilter) {
		this.searchFilter = searchFilter;
	}

	public List<Document> getDocuments() {
		return documents;
	}

	public void setDocuments(List<Document> documents) {
		this.documents = documents;
	}

}
