package com.firstrain.web.pojo;

import java.util.List;

import com.firstrain.frapi.util.DefaultEnums.RelevanceBand;

public class Entity {

	private String id;
	private String name;
	private String searchToken;
	private String relevanceBand;
	private Short relevanceScore;
	private List<Location> location;
	private Boolean removed;
	private Boolean added;
	private Boolean subscribed;
	private String message;
	private Integer errorCode;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSearchToken() {
		return searchToken;
	}

	public void setSearchToken(String searchToken) {
		this.searchToken = searchToken;
	}

	public String getRelevanceBand() {
		return relevanceBand;
	}

	public void setRelevanceBand(Short relevanceBand) {
		if (relevanceBand == null) {
			relevanceBand = RelevanceBand.LOW.getValue();
		}
		if (relevanceBand == RelevanceBand.HIGH.getValue()) {
			this.relevanceBand = RelevanceBand.HIGH.getName();
		} else if (relevanceBand == RelevanceBand.MEDIUM.getValue()) {
			this.relevanceBand = RelevanceBand.MEDIUM.getName();
		} else {
			this.relevanceBand = RelevanceBand.LOW.getName();
		}
	}

	public Short getRelevanceScore() {
		return relevanceScore;
	}

	public void setRelevanceScore(Short relevanceScore) {
		this.relevanceScore = relevanceScore;
	}

	public List<Location> getLocation() {
		return location;
	}

	public void setLocation(List<Location> location) {
		this.location = location;
	}

	public Boolean getRemoved() {
		return removed;
	}

	public void setRemoved(Boolean removed) {
		this.removed = removed;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Integer getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(Integer errorCode) {
		this.errorCode = errorCode;
	}

	public Boolean getAdded() {
		return added;
	}

	public void setAdded(Boolean added) {
		this.added = added;
	}

	public Boolean getSubscribed() {
		return subscribed;
	}

	public void setSubscribed(Boolean subscribed) {
		this.subscribed = subscribed;
	}
}
