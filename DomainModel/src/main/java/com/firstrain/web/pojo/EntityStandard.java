package com.firstrain.web.pojo;

import java.util.List;

public class EntityStandard {

	private String name;
	private String searchToken;
	private String relevanceBand;
	private Short relevanceScore;
	private String confidence;
	private String matchType;
	private Boolean removed;
	private Boolean added;
	private Boolean subscribed;
	private String message;
	private Integer errorCode;
	private com.firstrain.frapi.pojo.Entity sector;
	private com.firstrain.frapi.pojo.Entity segment;
	private com.firstrain.frapi.pojo.Entity industry;
	private String homePage;
	private List<EntityLink> entityLinks;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSearchToken() {
		return searchToken;
	}

	public void setSearchToken(String searchToken) {
		this.searchToken = searchToken;
	}

	public String getRelevanceBand() {
		return relevanceBand;
	}

	public void setRelevanceBand(String relevanceBand) {
		this.relevanceBand = relevanceBand;
	}

	public Short getRelevanceScore() {
		return relevanceScore;
	}

	public void setRelevanceScore(Short relevanceScore) {
		this.relevanceScore = relevanceScore;
	}

	public Boolean getRemoved() {
		return removed;
	}

	public void setRemoved(Boolean removed) {
		this.removed = removed;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Integer getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(Integer errorCode) {
		this.errorCode = errorCode;
	}

	public Boolean getAdded() {
		return added;
	}

	public void setAdded(Boolean added) {
		this.added = added;
	}

	public Boolean getSubscribed() {
		return subscribed;
	}

	public void setSubscribed(Boolean subscribed) {
		this.subscribed = subscribed;
	}

	public String getConfidence() {
		return confidence;
	}

	public void setConfidence(String confidence) {
		this.confidence = confidence;
	}

	public String getMatchType() {
		return matchType;
	}

	public void setMatchType(String matchType) {
		this.matchType = matchType;
	}

	public com.firstrain.frapi.pojo.Entity getSector() {
		return sector;
	}

	public void setSector(com.firstrain.frapi.pojo.Entity sector) {
		this.sector = sector;
	}

	public com.firstrain.frapi.pojo.Entity getSegment() {
		return segment;
	}

	public void setSegment(com.firstrain.frapi.pojo.Entity segment) {
		this.segment = segment;
	}

	public com.firstrain.frapi.pojo.Entity getIndustry() {
		return industry;
	}

	public void setIndustry(com.firstrain.frapi.pojo.Entity industry) {
		this.industry = industry;
	}

	public String getHomePage() {
		return homePage;
	}

	public List<EntityLink> getEntityLinks() {
		return entityLinks;
	}

	public void setEntityLinks(List<EntityLink> entityLinks) {
		this.entityLinks = entityLinks;
	}

	public void setHomePage(String homePage) {
		this.homePage = homePage;
	}
}
