package com.firstrain.web.pojo;

import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.annotate.JsonIgnore;

@XmlRootElement(name = "data")
public class EntityDataHtml {
	private String fr;
	private String ft;
	private String e;
	private String twt;
	private String bi;
	private String md;
	private String gl;
	private String rl;
	private String tt;
	private String wv;
	private Boolean frContentBi;
	private Boolean frContentMd;
	private Boolean frContentGl;
	private Boolean frContentRl;
	private Boolean frContentTwt;
	private Boolean frContentTt;
	private Boolean frContentWv;
	private String analyticsRibbon;
	private String mgmtTurnoverChart;
	private String document;
	private String tweet;

	public String getDocument() {
		return this.document;
	}

	public void setDocument(String document) {
		this.document = document;
	}

	public String getTweet() {
		return this.tweet;
	}

	public void setTweet(String tweet) {
		this.tweet = tweet;
	}

	public String getFr() {
		return fr;
	}

	public void setFr(String fr) {
		this.fr = fr;
	}

	public String getFt() {
		return ft;
	}

	public void setFt(String ft) {
		this.ft = ft;
	}

	public String getTwt() {
		return twt;
	}

	public void setTwt(String twt) {
		this.twt = twt;
	}

	public String getBi() {
		return bi;
	}

	public void setBi(String bi) {
		this.bi = bi;
	}

	public String getMd() {
		return md;
	}

	public void setMd(String md) {
		this.md = md;
	}

	public String getGl() {
		return gl;
	}

	public void setGl(String gl) {
		this.gl = gl;
	}

	public String getRl() {
		return rl;
	}

	public void setRl(String rl) {
		this.rl = rl;
	}

	public String getTt() {
		return tt;
	}

	public void setTt(String tt) {
		this.tt = tt;
	}

	@JsonIgnore
	public String getAnalyticsRibbon() {
		return analyticsRibbon;
	}

	public void setAnalyticsRibbon(String analyticsRibbon) {
		this.analyticsRibbon = analyticsRibbon;
	}

	public String getWv() {
		return wv;
	}

	public void setWv(String wv) {
		this.wv = wv;
	}

	public Boolean getFrContentWv() {
		return frContentWv;
	}

	public void setFrContentWv(Boolean frContentWv) {
		this.frContentWv = frContentWv;
	}

	@JsonIgnore
	public String getMgmtTurnoverChart() {
		return mgmtTurnoverChart;
	}

	public void setMgmtTurnoverChart(String mgmtTurnoverChart) {
		this.mgmtTurnoverChart = mgmtTurnoverChart;
	}

	public Boolean getFrContentBi() {
		return frContentBi;
	}

	public void setFrContentBi(Boolean frContentBi) {
		this.frContentBi = frContentBi;
	}

	public Boolean getFrContentMd() {
		return frContentMd;
	}

	public void setFrContentMd(Boolean frContentMd) {
		this.frContentMd = frContentMd;
	}

	public Boolean getFrContentGl() {
		return frContentGl;
	}

	public void setFrContentGl(Boolean frContentGl) {
		this.frContentGl = frContentGl;
	}

	public Boolean getFrContentRl() {
		return frContentRl;
	}

	public void setFrContentRl(Boolean frContentRl) {
		this.frContentRl = frContentRl;
	}

	public Boolean getFrContentTwt() {
		return frContentTwt;
	}

	public void setFrContentTwt(Boolean frContentTwt) {
		this.frContentTwt = frContentTwt;
	}

	public Boolean getFrContentTt() {
		return frContentTt;
	}

	public void setFrContentTt(Boolean frContentTt) {
		this.frContentTt = frContentTt;
	}

	public String getE() {
		return e;
	}

	public void setE(String e) {
		this.e = e;
	}
}
