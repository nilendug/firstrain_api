package com.firstrain.web.pojo;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "data")
public class MetaData {
	private String fr;
	private String ft;
	private String e;
	private String visualisation;
	private String twt;
	private String bi;
	private String md;
	private String gl;
	private String rl;
	private String tt;
	private String document;
	private String tweet;

	public String getDocument() {
		return this.document;
	}

	public void setDocument(String document) {
		this.document = document;
	}

	public String getTweet() {
		return this.tweet;
	}

	public void setTweet(String tweet) {
		this.tweet = tweet;
	}

	public String getFr() {
		return fr;
	}

	public void setFr(String fr) {
		this.fr = fr;
	}

	public String getFt() {
		return ft;
	}

	public void setFt(String ft) {
		this.ft = ft;
	}

	public String getVisualisation() {
		return visualisation;
	}

	public void setVisualisation(String visualisation) {
		this.visualisation = visualisation;
	}

	public String getTwt() {
		return twt;
	}

	public void setTwt(String twt) {
		this.twt = twt;
	}

	public String getBi() {
		return bi;
	}

	public void setBi(String bi) {
		this.bi = bi;
	}

	public String getMd() {
		return md;
	}

	public void setMd(String md) {
		this.md = md;
	}

	public String getGl() {
		return gl;
	}

	public void setGl(String gl) {
		this.gl = gl;
	}

	public String getRl() {
		return rl;
	}

	public void setRl(String rl) {
		this.rl = rl;
	}

	public String getTt() {
		return tt;
	}

	public void setTt(String tt) {
		this.tt = tt;
	}

	public String getE() {
		return e;
	}

	public void setE(String e) {
		this.e = e;
	}
}
