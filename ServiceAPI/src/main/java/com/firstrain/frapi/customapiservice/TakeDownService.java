package com.firstrain.frapi.customapiservice;

import org.springframework.stereotype.Service;

import com.firstrain.frapi.FRService;

@Service
public interface TakeDownService extends FRService {

	public int takeDownContent(long enterpriseId, long articleId) throws Exception;

}
