package com.firstrain.frapi.service.impl;

import java.io.InputStream;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

import com.firstrain.log4j.Log4jUtils;
import com.firstrain.utils.FR_Loader;

@Service
public class CompanyEndingWords {
	private final Logger LOG = Logger.getLogger(CompanyEndingWords.class);
	private Set<String> companyEndingWords;
	private Set<String> companyEndingWordsRegex;
	private final String st_THE_Regex = "^(?i)the\\s+[,'. ]*";

	public Set<String> getCompanyEndingWords() {
		if (companyEndingWords == null) {
			load();
		}
		return companyEndingWords;
	}

	public Set<String> getCompanyEndingWordsRegex() {
		if (companyEndingWordsRegex == null) {
			load();
		}
		return companyEndingWordsRegex;
	}

	private void load() {
		Set<String> words = new LinkedHashSet<String>();
		Set<String> wordsRegex = new LinkedHashSet<String>();
		try {
			InputStream in = FR_Loader.getResourceAsStream("resource/endings.xml");
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			XPathFactory xpf = XPathFactory.newInstance();
			DocumentBuilder builder = dbf.newDocumentBuilder();
			Document doc = builder.parse(in);
			XPath xp = xpf.newXPath();
			Object o = xp.evaluate("root/variants/variant", doc, XPathConstants.NODESET);
			NodeList solrNodes = (NodeList) o;
			for (int i = 0; i < solrNodes.getLength(); ++i) {
				Node n = solrNodes.item(i);
				Node c = n.getFirstChild();
				if (c instanceof Text) {
					Text e = (Text) c;
					String word = e.getData().toLowerCase();
					words.add(word);
					wordsRegex.add("(?i)[,'.& ]*" + Pattern.quote(word) + "$");
				}
			}
			LOG.info("Loaded <" + words.size() + "> company ending words from resource/endings.xml");
		} catch (Exception e) {
			LOG.error("Error while loading CompanyEndingWords form resource/endings.xml", e);
		}
		companyEndingWords = Collections.unmodifiableSet(words);
		companyEndingWordsRegex = Collections.unmodifiableSet(wordsRegex);
	}

	public String trimCompanyEndingWord(String s) {
		if (s != null && !s.isEmpty()) {
			for (String regex : this.getCompanyEndingWordsRegex()) {
				String label1 = s.replaceAll(regex, "");
				if (label1 != s) {
					s = label1;
					break;
				}
			}
			s = s.replaceAll(st_THE_Regex, "");
		}
		return s;
	}

	public static void main(String[] args) {
		Log4jUtils.initializeConsoleLogger(Level.DEBUG);
		String[] labels = {"the Siemens AG", "Novartis AG", "The ,Apple,Inc", "tHE Internations Business Machine Corporation",
				"thE ,'Microsoft Dynamics", "The Dow Chemical Company", "THERMOGENESIS CORP.", "The Furukawa Electric Co., Ltd.",
				"TheMicrosoftCorporation", "JPMorgan Chase & Co.", "JPMorgan Chase and Co."};

		for (String label : labels) {
			// System.out.println(trimCompanyEndingWord(label));
		}
	}

}
