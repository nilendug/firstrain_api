package com.firstrain.web.controller.staticdata;

import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.firstrain.utils.JSONUtility;
import com.firstrain.web.response.JSONResponse;
import com.firstrain.web.response.JSONResponse.ResStatus;
import com.firstrain.web.service.staticdata.Constant;
import com.firstrain.web.service.staticdata.StaticDataService;

@Controller
@RequestMapping(value = "/widget")
public class WidgetController {

	private static final Logger LOG = Logger.getLogger(WidgetController.class);

	@Autowired
	private StaticDataService staticDataService;

	@Value("${img.css.base.url}")
	private String imgCssURL;
	@Value("${js.base.url}")
	private String jsURL;


	@RequestMapping(value = "/config", method = RequestMethod.GET)
	public void getConfig(HttpServletResponse response, @RequestParam("callback") String callback) {

		response.setContentType("text/javascript; charset=UTF-8");
		JSONResponse<Map<String, String>> res = new JSONResponse<Map<String, String>>();
		try {
			Map<String, String> config = new HashMap<String, String>();
			config.put("imgCssURL", imgCssURL);
			config.put("jsURL", jsURL);
			config.put("version", Constant.getVersion());
			config.put("appName", Constant.getAppName());

			res.setResult(config);
			res.setStatus(ResStatus.SUCCESS);
		} catch (Exception e) {
			res.setStatus(ResStatus.ERROR);
			LOG.error(e.getMessage(), e);
		}
		try {
			PrintWriter out = response.getWriter();
			out.print(callback + "(" + JSONUtility.serialize(res) + ")");
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
	}

	@RequestMapping(value = "/graph", method = RequestMethod.GET)
	public void getGraph(HttpServletResponse response, @RequestParam("callback") String callback, @RequestParam("id") String id,
			@RequestParam(value = "chTypes", required = false,
					defaultValue = "TREE_MONITOR_SEARCH,TREE_COMPANY,TREE_TOPICS,ACC_METER,GEO_WORLD,GEO_US") String chTypes) {

		response.setContentType("text/javascript; charset=UTF-8");
		JSONResponse<Object> res = new JSONResponse<Object>();
		try {
			Object o = null;
			if (id.startsWith("M:")) {
				o = staticDataService.getDataObject("visualization.txt", Object.class);
			} else {
				o = staticDataService.getDataObject("visualization_entity.txt", Object.class);
			}
			res.setResult(((Map) o).get("data"));
			res.setStatus(ResStatus.SUCCESS);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			res.setStatus(ResStatus.ERROR);
		}
		try {
			PrintWriter out = response.getWriter();
			out.print(callback + "(" + JSONUtility.serialize(res) + ")");
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
	}

	@RequestMapping(method = RequestMethod.OPTIONS, value = "/*")
	public void manageOptions(HttpServletRequest req, HttpServletResponse res) {
		String origin = req.getHeader("Origin");
		res.addHeader("Access-Control-Allow-Origin", origin);
		res.addHeader("Access-Control-Allow-Methods", "GET");
		res.addHeader("Access-Control-Allow-Headers", "authKey,frUserId");
	}

}
