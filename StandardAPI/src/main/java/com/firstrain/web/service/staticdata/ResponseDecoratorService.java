package com.firstrain.web.service.staticdata;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.stereotype.Service;

import com.firstrain.frapi.domain.Document.DocQuote;
import com.firstrain.frapi.domain.EntityStatus;
import com.firstrain.frapi.domain.ItemDetail;
import com.firstrain.frapi.domain.MonitorBucketForTitle;
import com.firstrain.frapi.pojo.AuthAPIResponse;
import com.firstrain.frapi.pojo.EntityBriefInfo;
import com.firstrain.frapi.pojo.MonitorAPIResponse;
import com.firstrain.frapi.pojo.MonitorBriefDetail;
import com.firstrain.frapi.pojo.UserAPIResponse;
import com.firstrain.frapi.pojo.wrapper.DocumentSet;
import com.firstrain.frapi.pojo.wrapper.TweetSet;
import com.firstrain.frapi.util.DefaultEnums.RelevanceBand;
import com.firstrain.frapi.util.FRAPIConstant;
import com.firstrain.frapi.util.StatusCode;
import com.firstrain.utils.FR_ArrayUtils;
import com.firstrain.utils.JSONUtility;
import com.firstrain.web.controller.core.ItemController;
import com.firstrain.web.pojo.AuthKey;
import com.firstrain.web.pojo.Content;
import com.firstrain.web.pojo.Document;
import com.firstrain.web.pojo.EntityData;
import com.firstrain.web.pojo.EntityStandard;
import com.firstrain.web.pojo.ItemData;
import com.firstrain.web.pojo.Monitor;
import com.firstrain.web.pojo.MonitorConfig;
import com.firstrain.web.pojo.MonitorDetails;
import com.firstrain.web.pojo.MonitorInfo;
import com.firstrain.web.pojo.MonitorSearch;
import com.firstrain.web.pojo.Source;
import com.firstrain.web.pojo.Tweet;
import com.firstrain.web.pojo.User;
import com.firstrain.web.response.AuthKeyResponse;
import com.firstrain.web.response.EntityDataResponse;
import com.firstrain.web.response.EntityResponse;
import com.firstrain.web.response.ItemWrapperResponse;
import com.firstrain.web.response.JSONResponse;
import com.firstrain.web.response.JSONResponse.ResStatus;
import com.firstrain.web.response.MonitorConfigResponse;
import com.firstrain.web.response.MonitorDetailsResponse;
import com.firstrain.web.response.MonitorInfoResponse;
import com.firstrain.web.response.MonitorWrapperResponse;
import com.firstrain.web.response.UserWrapperResponse;
import com.firstrain.web.service.core.RequestParsingService.defaultSpec;
import com.firstrain.web.util.AuthAPIResponseThreadLocal;
import com.firstrain.web.wrapper.EntityDataWrapper;
import com.firstrain.web.wrapper.EntityWrapper;
import com.firstrain.web.wrapper.ItemWrapper;
import com.firstrain.web.wrapper.MonitorWrapper;
import com.firstrain.web.wrapper.UserWrapper;

@Service
public class ResponseDecoratorService {

	@Autowired
	private ResourceBundleMessageSource messageSource;
	private static final Logger LOG = Logger.getLogger(ItemController.class);
	@Value("${api.version}")
	private String version;

	public AuthKeyResponse getAuthKeyResponse(AuthAPIResponse apiRes) {
		AuthKeyResponse res = new AuthKeyResponse();
		res.setStatus(ResStatus.SUCCESS);
		String message = messageSource.getMessage(("authkey.succ"), null, Locale.getDefault());
		res.setMessage(message);
		res.setVersion(apiRes.getApiVersion());
		AuthKey data = new AuthKey();
		data.setAuthKey(apiRes.getAuthKey());
		// date in UTC - 18 Sep 2014 07:16:43 UTC
		SimpleDateFormat formater = new SimpleDateFormat("dd MMM yyyy HH:mm:ss z");
		formater.setTimeZone(TimeZone.getTimeZone("UTC"));
		String validTill = formater.format(new Date(apiRes.getExpiryTime().getTime()));
		data.setValidTill(validTill);
		res.setResult(data);
		return res;
	}

	public UserWrapperResponse getUserResponse(UserAPIResponse apiRes, String msgKey) {
		UserWrapperResponse res = new UserWrapperResponse();
		res.setStatus(ResStatus.SUCCESS);
		String message = messageSource.getMessage(msgKey, null, Locale.getDefault());
		res.setMessage(message);
		res.setVersion(version);
		UserWrapper data = new UserWrapper();
		User user = new User();
		data.setUser(user);
		res.setResult(data);
		// populate user
		com.firstrain.frapi.domain.User apiUser = apiRes.getUser();
		user.setuCompany(apiUser.getUserCompany());
		user.setuEmail(apiUser.getEmail());
		user.setuFirstName(apiUser.getFirstName());
		user.setuLastName(apiUser.getLastName());
		user.setUserId(FRAPIConstant.USER_ID_PREFIX + apiUser.getUserId());
		user.setuStatus(apiUser.getFlags());
		user.setuTimezone(apiUser.getTimeZone());
		return res;
	}

	public EntityResponse getMonitorEntityResponse(EntityStatus estatus) {
		EntityResponse res = new EntityResponse();
		res.setStatus(ResStatus.SUCCESS);
		String message = messageSource.getMessage("gen.succ", null, Locale.getDefault());
		res.setMessage(message);
		res.setVersion(version);
		EntityWrapper data = new EntityWrapper();

		EntityStandard entity = new EntityStandard();
		com.firstrain.frapi.pojo.Entity ent = estatus.getEntity();
		entity.setSearchToken(ent.getSearchToken());
		entity.setName(ent.getName());
		entity.setSubscribed(estatus.getEntityStatus());
		data.setEntity(entity);

		data.setId(FRAPIConstant.MONITOR_PREFIX + estatus.getId());
		data.setName(estatus.getName());

		res.setResult(data);
		return res;
	}

	public MonitorInfoResponse getAddRemoveEntityResponse(MonitorAPIResponse apiRes, String msgKey) {
		MonitorInfoResponse res = new MonitorInfoResponse();
		res.setStatus(ResStatus.SUCCESS);
		String message = messageSource.getMessage(msgKey, null, Locale.getDefault());
		res.setVersion(version);
		MonitorInfo data = new MonitorInfo();
		data.setMonitorId(FRAPIConstant.MONITOR_PREFIX + Long.toString(apiRes.getMonitorId()));
		data.setMonitorName(apiRes.getMonitorName());
		if (apiRes.getEntities() != null) {
			List<EntityWrapper> entities = new ArrayList<EntityWrapper>();
			data.setEntities(entities);
			for (com.firstrain.frapi.pojo.Entity ent : apiRes.getEntities()) {
				EntityStandard entity = new EntityStandard();
				entity.setName(ent.getName());
				entity.setSearchToken(ent.getSearchToken());
				entity.setRemoved(ent.getRemoved());
				entity.setAdded(ent.getAdded());
				if (Boolean.FALSE.equals(ent.getRemoved())) {
					entity.setErrorCode(StatusCode.RSOURCE_NOT_FOUND);
					entity.setMessage(
							messageSource.getMessage("entity.errorcode." + StatusCode.RSOURCE_NOT_FOUND, null, Locale.getDefault()));
					message = messageSource.getMessage("errorcode." + StatusCode.PARTIAL_SUCCESS, null, Locale.getDefault());
					res.setStatus(ResStatus.PARTIAL_SUCCESS);
					res.setErrorCode(StatusCode.PARTIAL_SUCCESS);
				} else if (ent.getRemoved() == null && ent.getAdded() == null) {
					entity.setErrorCode(StatusCode.RSOURCE_NOT_FOUND);
					entity.setMessage(
							messageSource.getMessage("entity.errorcode." + StatusCode.RSOURCE_NOT_FOUND, null, Locale.getDefault()));
					message = messageSource.getMessage("errorcode." + StatusCode.PARTIAL_SUCCESS, null, Locale.getDefault());
					res.setStatus(ResStatus.PARTIAL_SUCCESS);
					res.setErrorCode(StatusCode.PARTIAL_SUCCESS);
				} else if (Boolean.FALSE.equals(ent.getAdded())) {
					entity.setErrorCode(StatusCode.ENTITY_ALREADY_EXISTS);
					entity.setMessage(messageSource.getMessage("errorcode." + StatusCode.ENTITY_ALREADY_EXISTS, null, Locale.getDefault()));
					message = messageSource.getMessage("errorcode." + StatusCode.PARTIAL_SUCCESS, null, Locale.getDefault());
					res.setStatus(ResStatus.PARTIAL_SUCCESS);
					res.setErrorCode(StatusCode.PARTIAL_SUCCESS);
				}
				EntityWrapper entityWrapper = new EntityWrapper();
				entityWrapper.setEntity(entity);
				entities.add(entityWrapper);
			}
		}
		res.setMessage(message);
		res.setResult(data);
		return res;
	}

	public MonitorConfigResponse getMonitorConfigResponse(com.firstrain.frapi.domain.MonitorConfig apiRes) {
		MonitorConfigResponse res = new MonitorConfigResponse();
		res.setStatus(ResStatus.SUCCESS);
		String message = messageSource.getMessage("gen.succ", null, Locale.getDefault());
		res.setVersion(version);
		res.setMessage(message);

		MonitorConfig data = new MonitorConfig();
		data.setId(FRAPIConstant.MONITOR_PREFIX + apiRes.getMonitorId());
		data.setName(apiRes.getMonitorName());
		data.setFilters(apiRes.getFilters());
		if (apiRes.getQueries() != null) {
			List<MonitorSearch> queries = new ArrayList<MonitorSearch>();
			data.setQueries(queries);
			for (ItemDetail item : apiRes.getQueries()) {
				MonitorSearch search = new MonitorSearch();
				search.setQueryName(item.getQueryName());
				search.setQueryString(item.getQueryString());
				queries.add(search);
			}
		}
		res.setResult(data);
		return res;
	}

	public MonitorDetailsResponse getMonitorDetailsResponse(com.firstrain.frapi.domain.MonitorDetails monitorDetailsServiceApi) {
		MonitorDetailsResponse res = new MonitorDetailsResponse();
		res.setStatus(ResStatus.SUCCESS);
		String message = messageSource.getMessage("user.monitors", null, Locale.getDefault());
		res.setMessage(message);
		res.setVersion(version);

		MonitorDetails monitorDetails = new MonitorDetails();
		res.setResult(monitorDetails);


		List<MonitorBucketForTitle> monitorBucketForTitleLst = monitorDetailsServiceApi.getTitlesForMonitorBuckets();
		LinkedHashMap<String, List<com.firstrain.frapi.domain.MonitorInfo>> monitorDetailsLst = monitorDetailsServiceApi.getMonitors();

		LinkedHashMap<String, List<MonitorInfo>> monitors = new LinkedHashMap<String, List<MonitorInfo>>();
		if (monitorBucketForTitleLst != null && !monitorBucketForTitleLst.isEmpty()) {
			for (MonitorBucketForTitle monitorBucketForTitle : monitorBucketForTitleLst) {
				String monitorBucket = monitorBucketForTitle.getMonitorBucket();
				List<com.firstrain.frapi.domain.MonitorInfo> monitorInfoServiceApiLst = monitorDetailsLst.get(monitorBucket);
				List<MonitorInfo> monitorInfoLst = new ArrayList<MonitorInfo>();

				if (monitorInfoServiceApiLst != null && !monitorInfoServiceApiLst.isEmpty()) {
					for (com.firstrain.frapi.domain.MonitorInfo monitorInfoServiceApi : monitorInfoServiceApiLst) {
						MonitorInfo monitorInfo = new MonitorInfo();
						monitorInfo.setActiveMail(monitorInfoServiceApi.getMailAvailable());
						// monitorInfo.setFavorite(monitorInfoServiceApi.isFavorite());
						monitorInfo.setFavoriteItemId(monitorInfoServiceApi.getFavoriteUserItemId());
						// monitorInfo.setMonitorAdmin(monitorInfoServiceApi.isMonitorAdmin());
						monitorInfo.setMonitorId(monitorInfoServiceApi.getMonitorId());
						monitorInfo.setMonitorName(monitorInfoServiceApi.getMonitorName());
						monitorInfoLst.add(monitorInfo);
					}
				}
				monitors.put(monitorBucket, monitorInfoLst);

				// Fill MonitorDetails Object
				monitorDetails.setMonitors(monitors);
				// monitorDetails.setUser(monitorDetailsServiceApi.getUserName());
				// monitorDetails.setUserId(monitorDetailsServiceApi.getUserId());
			}
		}

		return res;
	}

	public ItemWrapperResponse getItemWrapperResponse(DocumentSet documentSet, String msgKey) {

		ItemWrapperResponse res = new ItemWrapperResponse();
		res.setStatus(ResStatus.SUCCESS);
		String message = messageSource.getMessage(msgKey, null, Locale.getDefault());
		res.setMessage(message);
		res.setVersion(version);

		ItemWrapper itemWrapper = new ItemWrapper();
		ItemData itemData = new ItemData();
		itemData.setDocument(documentConvertor(documentSet.getDocuments().get(0)));
		itemWrapper.setData(itemData);
		res.setResult(itemWrapper);
		return res;
	}

	private String setRelevanceBand(Integer relevanceBand) {
		String relevanceBandStr;
		if (relevanceBand == null) {
			relevanceBand = (int) RelevanceBand.LOW.getValue();
		}
		if (relevanceBand == RelevanceBand.HIGH.getValue()) {
			relevanceBandStr = RelevanceBand.HIGH.getName();
		} else if (relevanceBand == RelevanceBand.MEDIUM.getValue()) {
			relevanceBandStr = RelevanceBand.MEDIUM.getName();
		} else {
			relevanceBandStr = RelevanceBand.LOW.getName();
		}
		return relevanceBandStr;
	}

	public ItemWrapperResponse getItemWrapperResponse(TweetSet tweetSet, String msgKey) {

		ItemWrapperResponse res = new ItemWrapperResponse();
		res.setStatus(ResStatus.SUCCESS);
		String message = messageSource.getMessage(msgKey, null, Locale.getDefault());
		res.setMessage(message);
		res.setVersion(version);

		ItemWrapper itemWrapper = new ItemWrapper();
		ItemData itemData = new ItemData();
		com.firstrain.frapi.domain.Tweet tweetRst = tweetSet.getTweets().get(0);
		itemData.setTweet(tweetConvertor(tweetRst));
		itemWrapper.setData(itemData);
		res.setResult(itemWrapper);
		return res;
	}

	public MonitorInfoResponse getMonitorInfoResponse(MonitorAPIResponse monitorApiRes, String msgKey) {
		MonitorInfoResponse res = new MonitorInfoResponse();
		res.setStatus(ResStatus.SUCCESS);
		String message = messageSource.getMessage(msgKey, null, Locale.getDefault());
		res.setMessage(message);
		res.setVersion(version);

		MonitorInfo monitorInfo = new MonitorInfo();
		res.setResult(monitorInfo);

		monitorInfo.setMonitorId(Long.toString(monitorApiRes.getMonitorId()));
		monitorInfo.setMonitorName(monitorApiRes.getMonitorName());

		return res;
	}

	public MonitorWrapperResponse getMonitorWrapperResponse(MonitorAPIResponse monitorApiRes, String msgKey) {
		MonitorWrapperResponse res = new MonitorWrapperResponse();
		res.setStatus(ResStatus.SUCCESS);
		String message = messageSource.getMessage(msgKey, null, Locale.getDefault());
		res.setMessage(message);
		res.setVersion(version);

		MonitorWrapper monitorWrapper = new MonitorWrapper();
		res.setResult(monitorWrapper);

		Monitor monitor = new Monitor();
		monitorWrapper.setMonitor(monitor);
		monitor.setId(FRAPIConstant.MONITOR_PREFIX + monitorApiRes.getMonitorId());
		monitor.setName(monitorApiRes.getMonitorName());
		return res;
	}

	public EntityDataResponse getEntityDataResponse(MonitorAPIResponse monitorApiRes, String msgKey) {

		EntityDataResponse res = new EntityDataResponse();
		res.setStatus(ResStatus.SUCCESS);
		String message = messageSource.getMessage(msgKey, null, Locale.getDefault());
		res.setMessage(message);
		res.setVersion(version);

		EntityDataWrapper entityDataWrapper = new EntityDataWrapper();
		EntityData entityData = new EntityData();
		MonitorBriefDetail monitorBriefDetail = monitorApiRes.getMonitorBriefDetail();
		if (monitorBriefDetail != null) {
			// set Content Document
			com.firstrain.frapi.pojo.wrapper.DocumentSet documentSet = monitorBriefDetail.getWebResults();
			if (documentSet != null) {
				List<com.firstrain.frapi.domain.Document> documentLstRst = documentSet.getDocuments();
				if (documentLstRst != null && !documentLstRst.isEmpty()) {
					Content content = new Content();
					List<Document> documentLst = new ArrayList<Document>();
					for (com.firstrain.frapi.domain.Document documentRst : documentLstRst) {
						documentLst.add(documentConvertor(documentRst));
					}
					content.setDocuments(documentLst);
					entityData.setFr(content);
				}
			}
			// set Content tweets
			com.firstrain.frapi.pojo.wrapper.TweetSet tweetSet = monitorBriefDetail.getTweetList();
			if (tweetSet != null) {
				List<com.firstrain.frapi.domain.Tweet> tweetLstRst = tweetSet.getTweets();
				if (tweetLstRst != null && !tweetLstRst.isEmpty()) {
					Content content = new Content();
					List<Tweet> tweetLst = new ArrayList<Tweet>();
					for (com.firstrain.frapi.domain.Tweet tweetRst : tweetLstRst) {
						tweetLst.add(tweetConvertor(tweetRst));
					}
					content.setTweets(tweetLst);
					entityData.setFt(content);
				}
			}
		}
		entityDataWrapper.setData(entityData);
		res.setResult(entityDataWrapper);
		return res;
	}

	public EntityDataResponse getEntityDataResponse(EntityBriefInfo entityBriefInfo, String msgKey) {

		EntityDataResponse res = new EntityDataResponse();
		res.setStatus(ResStatus.SUCCESS);
		String message = messageSource.getMessage(msgKey, null, Locale.getDefault());
		res.setMessage(message);
		res.setVersion(version);

		EntityDataWrapper entityDataWrapper = new EntityDataWrapper();
		EntityData entityData = new EntityData();

		// set Content Document
		com.firstrain.frapi.pojo.wrapper.DocumentSet documentSet = entityBriefInfo.getWebResults();
		if (documentSet != null) {
			List<com.firstrain.frapi.domain.Document> documentLstRst = documentSet.getDocuments();
			if (documentLstRst != null && !documentLstRst.isEmpty()) {
				Content content = new Content();
				List<Document> documentLst = new ArrayList<Document>();
				for (com.firstrain.frapi.domain.Document documentRst : documentLstRst) {
					documentLst.add(documentConvertor(documentRst));
				}
				content.setDocuments(documentLst);
				entityData.setFr(content);
			}
		}
		// set Content tweets
		com.firstrain.frapi.pojo.wrapper.TweetSet tweetSet = entityBriefInfo.getTweetList();
		if (tweetSet != null) {
			List<com.firstrain.frapi.domain.Tweet> tweetLstRst = tweetSet.getTweets();
			if (tweetLstRst != null && !tweetLstRst.isEmpty()) {
				Content content = new Content();
				List<Tweet> tweetLst = new ArrayList<Tweet>();
				for (com.firstrain.frapi.domain.Tweet tweetRst : tweetLstRst) {
					tweetLst.add(tweetConvertor(tweetRst));
				}
				content.setTweets(tweetLst);
				entityData.setFt(content);
			}
		}

		entityDataWrapper.setData(entityData);
		res.setResult(entityDataWrapper);
		return res;
	}

	@SuppressWarnings("rawtypes")
	public JSONResponse getSuccessMsg(String msgKey) {
		JSONResponse res = new JSONResponse();
		res.setStatus(ResStatus.SUCCESS);
		String message = messageSource.getMessage(msgKey, null, Locale.getDefault());
		res.setMessage(message);
		res.setVersion(version);
		return res;
	}

	private String getFormatTime(Date date) {
		// date in UTC - 18 Sep 2014 07:16:43 UTC
		SimpleDateFormat formater = new SimpleDateFormat("dd MMM yyyy HH:mm:ss z");
		formater.setTimeZone(TimeZone.getTimeZone("UTC"));
		return formater.format(date);
	}

	private Tweet tweetConvertor(com.firstrain.frapi.domain.Tweet tweetRst) {

		if (tweetRst == null) {
			return null;
		}

		Tweet tweet = new Tweet();
		tweet.setAuthorName(tweetRst.getName());
		tweet.setAuthorDescription(tweetRst.getDescription());
		com.firstrain.frapi.pojo.Entity entity = tweetRst.getEntity();
		if (entity != null) {
			EntityStandard e = new EntityStandard();
			String name = entity.getName();
			e.setName(name);
			e.setSearchToken(entity.getSearchToken());
			tweet.setEntity(e);
		}
		tweet.setTweetHtml(tweetRst.getTitle());
		tweet.setTweetId(tweetRst.getTweetId());
		Date date = tweetRst.getTweetCreationDate();
		if (date != null) {
			tweet.setTimeStamp(getFormatTime(date));
		}
		tweet.setLink((List<String>) tweetRst.getExpandedLinks());
		tweet.setUserName(tweetRst.getScreenName());
		tweet.setTitle(tweetRst.getCoreTweet());
		tweet.setTweetText(tweetRst.getTweet());
		tweet.setAuthorAvatar(tweetRst.getUserImage());

		return tweet;
	}

	private Document documentConvertor(com.firstrain.frapi.domain.Document documentRst) {

		if (documentRst == null) {
			return null;
		}
		Document document = new Document();
		document.setId(documentRst.getId());
		Date date = documentRst.getDate();
		if (date != null) {
			document.setTimeStamp(getFormatTime(date));
		}
		if (documentRst.getContentType() != null) {
			document.setContentType(documentRst.getContentType().name());
		}

		com.firstrain.frapi.pojo.Entity entity = documentRst.getSource();
		if (entity != null) {
			Source source = new Source();
			source.setName(entity.getName());
			source.setSearchToken(entity.getSearchToken());
			document.setSource(source);
		}

		List<com.firstrain.frapi.pojo.Entity> entitiesList = documentRst.getCatEntries();
		if (entitiesList != null && !entitiesList.isEmpty()) {
			List<Integer> topicDim = getTopicDimensions();
			List<EntityStandard> entities = new ArrayList<EntityStandard>();
			for (com.firstrain.frapi.pojo.Entity e1 : entitiesList) {
				if (topicDim != null && topicDim.contains(e1.getType())) {
					EntityStandard e = new EntityStandard();
					String name = e1.getName();
					e.setName(name);
					e.setSearchToken(e1.getSearchToken());
					e.setRelevanceBand(setRelevanceBand(e1.getBand()));
					e.setRelevanceScore(e1.getRelevanceScore());
					entities.add(e);
				}
			}
			if (!entities.isEmpty()) {
				document.setEntity(entities);
			}
		}
		document.setTitle(documentRst.getTitle());
		document.setSnippet(documentRst.getSummary());
		document.setLink(documentRst.getUrl());
		DocQuote docQuote = documentRst.getDocQuote();
		if (docQuote != null) {
			document.setQuotes(docQuote.getHighlightedQuote());
		}
		document.setImage(documentRst.getImage());

		return document;
	}

	private List<Integer> getTopicDimensions() {
		List<Integer> topicDimensions = new ArrayList<Integer>();
		AuthAPIResponse authAPIResponse = AuthAPIResponseThreadLocal.get();
		String specJson = authAPIResponse.getPrefJson();
		if (specJson != null) {
			try {
				Map<String, Object> specParam = JSONUtility.deserialize(specJson, Map.class);
				Object tpdCSV = specParam.get(defaultSpec.TOPIC_DIMENSION_FOR_TAGGING);
				if (tpdCSV != null) {
					topicDimensions = FR_ArrayUtils.csvToIntegerList(tpdCSV.toString());
				}
			} catch (Exception e) {
				LOG.error(e.getMessage(), e);
			}
		}
		return topicDimensions;
	}

}
