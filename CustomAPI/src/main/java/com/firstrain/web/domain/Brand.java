package com.firstrain.web.domain;

import java.util.Map;

public class Brand {

	private Map<String, PwToken> pwTokenMap;
	private Map<Integer, Group> groupMap;
	private Map<String, Topic> topicMap;

	public Map<String, PwToken> getPwTokenMap() {
		return pwTokenMap;
	}

	public void setPwTokenMap(Map<String, PwToken> pwTokenMap) {
		this.pwTokenMap = pwTokenMap;
	}

	public Map<Integer, Group> getGroupMap() {
		return groupMap;
	}

	public void setGroupMap(Map<Integer, Group> groupMap) {
		this.groupMap = groupMap;
	}

	public Map<String, Topic> getTopicMap() {
		return topicMap;
	}

	public void setTopicMap(Map<String, Topic> topicMap) {
		this.topicMap = topicMap;
	}


}
