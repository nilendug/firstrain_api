package com.firstrain.web.domain;

import java.util.Map;

import com.firstrain.web.pojo.CategorizerObject.CatEntity;

public class MultipleMatchedEntities {

	private Map<String, CatEntity> matchedTaxonomyMap;
	private Map<String, CatEntity> companyMap;

	public Map<String, CatEntity> getMatchedTaxonomyMap() {
		return matchedTaxonomyMap;
	}

	public void setMatchedTaxonomyMap(Map<String, CatEntity> matchedTaxonomyMap) {
		this.matchedTaxonomyMap = matchedTaxonomyMap;
	}

	public Map<String, CatEntity> getCompanyMap() {
		return companyMap;
	}

	public void setCompanyMap(Map<String, CatEntity> companyMap) {
		this.companyMap = companyMap;
	}
}
