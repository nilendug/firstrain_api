package com.firstrain.web.service.core;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.stereotype.Service;

import com.firstrain.frapi.util.StatusCode;
import com.firstrain.web.domain.Brand;
import com.firstrain.web.domain.Group;
import com.firstrain.web.domain.MultipleMatchedEntities;
import com.firstrain.web.domain.PwToken;
import com.firstrain.web.domain.Topic;
import com.firstrain.web.pojo.CategorizerObject;
import com.firstrain.web.pojo.CategorizerObject.Attribute;
import com.firstrain.web.pojo.CategorizerObject.CatEntity;
import com.firstrain.web.pojo.CategorizerObject.CatEntityWrapper;
import com.firstrain.web.pojo.Entity;
import com.firstrain.web.pojo.Location;
import com.firstrain.web.response.CategorizationServiceResponse;
import com.firstrain.web.response.JSONResponse;
import com.firstrain.web.response.JSONResponse.ResStatus;
import com.firstrain.web.wrapper.EntityListWrapper;
import com.firstrain.web.wrapper.EntityListWrapperData;
import com.google.common.collect.Lists;

@Service
public class ResponseDecoratorService {

	private static final Logger LOG = Logger.getLogger(ResponseDecoratorService.class);
	private static final String I_COMPANIES = "I:PWCompanies";
	private static final String SEPRATOR = "C:";
	private static final String COMPANIES = "companies";

	@Autowired
	private ResourceBundleMessageSource messageSource;
	@Value("${api.version}")
	private String version;

	public String getVersion() {
		return version;
	}

	public CategorizationServiceResponse getEntityListResponse(CategorizerObject categorizeServiceResponse, String guid) {
		CategorizationServiceResponse res = new CategorizationServiceResponse();
		EntityListWrapperData resp = new EntityListWrapperData();
		res.setResult(resp);
		// set guid
		resp.setGuid(guid);
		res.setVersion(version);

		String messageKey = null;
		if (categorizeServiceResponse != null && categorizeServiceResponse.getResponseCode() == 200) {

			CatEntityWrapper catEntityWrapper = categorizeServiceResponse.getData();
			List<Entity> entityList = null;
			if (catEntityWrapper != null) {
				entityList = getEntityList(categorizeServiceResponse.getData().getCategorizerResponse());

				if (CollectionUtils.isNotEmpty(entityList)) {
					EntityListWrapper ewrap = new EntityListWrapper();
					ewrap.setEntity(entityList);
					resp.setData(ewrap);
					messageKey = "catservice.success";
					res.setStatus(ResStatus.SUCCESS);
				}
			}

			if (catEntityWrapper == null || CollectionUtils.isEmpty(entityList)) {
				messageKey = "catservice.nodata";
				res.setStatus(ResStatus.SUCCESS);
			}

		} else {
			messageKey = "errorcode." + StatusCode.SERVICE_NOT_AVAIL;
			res.setErrorCode(StatusCode.SERVICE_NOT_AVAIL);
			res.setStatus(ResStatus.ERROR);
		}
		res.setMessage(messageSource.getMessage(messageKey, null, Locale.getDefault()));
		return res;
	}

	public CategorizationServiceResponse getEntityListResponseForCategorizePw(CategorizerObject categorizeServiceResponse, String guid,
			Brand brand, List<String> taxonomyDirectives, String brandKey) {
		CategorizationServiceResponse res = new CategorizationServiceResponse();
		EntityListWrapperData resp = new EntityListWrapperData();
		res.setResult(resp);
		// set guid
		resp.setDocId(guid);
		res.setVersion(version);

		String messageKey = null;
		if (categorizeServiceResponse != null && categorizeServiceResponse.getResponseCode() == 200) {

			CatEntityWrapper catEntityWrapper = categorizeServiceResponse.getData();
			List<Entity> entityList = null;
			if (catEntityWrapper != null) {

				List<CatEntity> catEntities = catEntityWrapper.getCategorizerResponse();

				Set<String> pwTokens = new HashSet<String>();
				MultipleMatchedEntities multipleMatchedEntities = populateMultipleMatchedEntitiesMap(catEntities, brand.getTopicMap(), pwTokens);
				Set<String> finallyMatchedEntities = new HashSet<String>();
				Map<String, CatEntity> finalCatEntityMap = populateMatchedTaxonomyCatEntities(pwTokens, finallyMatchedEntities, brand,
						multipleMatchedEntities, taxonomyDirectives, brandKey);

				int i = 0;
				List<String> taxonomyDirectiveLst = null;
				for (String taxonomyDirective : taxonomyDirectives) {

					if (i == 0) {
						taxonomyDirectiveLst = resp.getTaxonomyDirective();
						if (taxonomyDirectiveLst == null) {
							taxonomyDirectiveLst = new ArrayList<String>();
							resp.setTaxonomyDirective(taxonomyDirectiveLst);
						}
						taxonomyDirectiveLst.add(taxonomyDirective);
						i++;
						continue;
					}

					taxonomyDirectiveLst.add(taxonomyDirective);
					// populate Companies
					if (COMPANIES.equalsIgnoreCase(taxonomyDirective)) {
						if (finalCatEntityMap == null) {
							finalCatEntityMap = new HashMap<String, CatEntity>();
						}
						populateExtraFrCompanyCategories(finalCatEntityMap, finallyMatchedEntities, multipleMatchedEntities.getCompanyMap());
					}

					i++;
				}

				if (MapUtils.isNotEmpty(finalCatEntityMap) && CollectionUtils.isNotEmpty(finalCatEntityMap.values())) {
					Collection<CatEntity> catEntitiesCol = finalCatEntityMap.values();
					List<CatEntity> lists = Lists.newArrayList(catEntitiesCol);
					Collections.sort(lists);
					entityList = getEntityListPw(lists);
					if (CollectionUtils.isNotEmpty(entityList)) {
						EntityListWrapper ewrap = new EntityListWrapper();
						ewrap.setEntities(entityList);
						resp.setData(ewrap);
						messageKey = "catservice.success";
						res.setStatus(ResStatus.SUCCESS);
					}
				}
			}

			if (catEntityWrapper == null || CollectionUtils.isEmpty(entityList)) {
				messageKey = "catservice.nodata";
				res.setStatus(ResStatus.SUCCESS);
			}

		} else {
			messageKey = "errorcode." + StatusCode.SERVICE_NOT_AVAIL;
			res.setErrorCode(StatusCode.SERVICE_NOT_AVAIL);
			res.setStatus(ResStatus.ERROR);
		}
		res.setMessage(messageSource.getMessage(messageKey, null, Locale.getDefault()));
		return res;
	}

	private Void populateExtraFrCompanyCategories(Map<String, CatEntity> finalCatEntityMap, Set<String> finallyMatchedEntities,
			Map<String, CatEntity> companyMap) {

		if (MapUtils.isEmpty(companyMap)) {
			return null;
		}

		for (Map.Entry<String, CatEntity> map : companyMap.entrySet()) {

			String key = map.getKey();
			CatEntity catEntity = map.getValue();

			if (finallyMatchedEntities.contains(key)) {
				continue;
			}

			Attribute attribute = catEntity.getAttribute();
			Short band = catEntity.getBand();
			Short score = catEntity.getScore();

			String searchToken = attribute.getAttrSearchToken();
			Long charOffset = catEntity.getCharOffset();
			Long charCount = catEntity.getCharCount();

			LOG.info("populating Fr Company Categories, searchToken :" + searchToken + ", band : " + band + ", score : " + score
					+ ", charOffset : " + charOffset + ", charCount : " + charCount);

			finalCatEntityMap.put(key, getCatEntity(searchToken, attribute.getName(), band != null ? band : (short) -1,
					score != null ? score : (short) -1, charOffset, charCount));

		}


		return null;
	}

	private List<Entity> getEntityListPw(List<CatEntity> catEntities) {

		if (CollectionUtils.isEmpty(catEntities)) {
			return null;
		}

		List<Entity> entityList = new ArrayList<Entity>();
		for (CatEntity catEntity : catEntities) {
			if (Boolean.TRUE.equals(catEntity.getAttrExclude())) {
				continue;
			}
			Entity entity = new Entity();
			entity.setName(catEntity.getAttribute().getName());
			entity.setRelevanceBand(catEntity.getBand());
			entity.setSearchToken(catEntity.getAttribute().getAttrSearchToken());
			entity.setRelevanceScore(catEntity.getScore());
			List<Location> location = entity.getLocation();
			if (location == null) {
				location = new ArrayList<Location>();
				entity.setLocation(location);
			}
			if (catEntity.getCharCount() != null) {
				Location loc = new Location();
				loc.setCharCount(catEntity.getCharCount());
				loc.setCharOffset(catEntity.getCharOffset());
				location.add(loc);
			}
			entityList.add(entity);
		}

		LOG.info("final entity list in response size : " + entityList.size());
		return entityList;
	}

	private List<Entity> getEntityList(List<CatEntity> catEntities) {

		if (CollectionUtils.isEmpty(catEntities)) {
			return null;
		}

		List<Entity> entityList = new ArrayList<Entity>();
		for (CatEntity catEntity : catEntities) {
			if (Boolean.TRUE.equals(catEntity.getAttrExclude())) {
				continue;
			}
			Entity entity = new Entity();
			entity.setName(catEntity.getAttribute().getName());
			entity.setRelevanceBand(catEntity.getBand());
			entity.setSearchToken(catEntity.getAttribute().getAttrSearchToken());
			entity.setRelevanceScore(catEntity.getScore());
			entityList.add(entity);
		}
		return entityList;
	}

	private Map<String, CatEntity> populateMatchedTaxonomyCatEntities(Set<String> pwTokens, Set<String> finallyMatchedEntities, Brand brand,
			MultipleMatchedEntities multipleMatchedEntities, List<String> taxonomyDirectives, String brandKey) {

		if (CollectionUtils.isEmpty(pwTokens) || MapUtils.isEmpty(brand.getPwTokenMap()) || MapUtils.isEmpty(brand.getGroupMap())) {
			return null;
		}

		Map<String, PwToken> pwTokenMap = brand.getPwTokenMap();
		Map<Integer, Group> groupMap = brand.getGroupMap();

		Map<String, CatEntity> matchedTaxonomyMap = multipleMatchedEntities.getMatchedTaxonomyMap();

		Map<String, CatEntity> catEntityMap = new HashMap<String, CatEntity>();
		for (String pwTokenStr : pwTokens) {
			PwToken pwToken = pwTokenMap.get(pwTokenStr);

			if (pwToken == null || CollectionUtils.isEmpty(pwToken.getGroups())) {
				continue;
			}

			Set<Integer> groups = pwToken.getGroups();

			LOG.debug("pwTokenStr : " + pwTokenStr + ", groups : " + groups);
			Short band = -1;
			Short score = -1;
			Long charOffset = null;
			Map<Long, Long> charMap = new HashMap<Long, Long>();

			for (Integer groupInt : groups) {

				Group group = groupMap.get(groupInt);
				CatEntity catEntity = getQualifyCatEntity(group, matchedTaxonomyMap, finallyMatchedEntities);

				if (catEntity == null) {
					continue;
				}

				Long charOffsetRes = catEntity.getCharOffset();
				if (charOffsetRes != null) {
					charMap.put(charOffsetRes, catEntity.getCharCount());
				}

				LOG.debug("in response find max , token : " + pwTokenStr + ", charOffset : " + charOffsetRes + ", charCount : "
						+ catEntity.getCharCount() + ", band : " + catEntity.getBand() + ", score : " + catEntity.getScore());

				charOffset = getMaxCharOffset(charOffset, charOffsetRes);
				band = getMaxBand(band, catEntity.getBand() != null ? catEntity.getBand() : -1);
				score = getMaxScore(score, catEntity.getScore() != null ? catEntity.getScore() : -1);

				if (I_COMPANIES.equalsIgnoreCase(pwTokenStr)
						&& (taxonomyDirectives.contains(brandKey) || taxonomyDirectives.contains(brandKey + "Companies"))) {
					Attribute attribute = catEntity.getAttribute();
					String searchToken = attribute.getAttrSearchToken();

					LOG.debug("finally to process for max -> searchToken : " + searchToken + ", band : " + band + ", score : " + score
							+ ", charOffset : " + charOffset + ", charCount : " + charMap.get(charOffset));

					catEntityMap.put(searchToken, getCatEntity(searchToken, attribute.getName(), band, score, charOffset, charMap.get(charOffset)));
				}

			}

			if (!I_COMPANIES.equalsIgnoreCase(pwTokenStr)
					&& (taxonomyDirectives.contains(brandKey) || taxonomyDirectives.contains(brandKey + "Topics"))) {

				LOG.debug("finally to process for max -> pwTokenStr : " + pwTokenStr + ", band : " + band + ", score : " + score + ", charOffset : "
						+ charOffset + ", charCount : " + charMap.get(charOffset));

				catEntityMap.put(pwTokenStr, getCatEntity(pwTokenStr, pwToken.getName(), band, score, charOffset, charMap.get(charOffset)));
			}
		}

		LOG.info("Returing Matched Taxonomy Entities Map Size : " + catEntityMap.size());
		return catEntityMap;
	}

	private Short getMaxScore(Short score, short score2) {
		return (short) Math.max(score, score2);
	}

	private Short getMaxBand(Short band, short band2) {
		return (short) Math.max(band, band2);
	}

	private Long getMaxCharOffset(Long charOffsetOld, Long charOffsetNew) {
		if (charOffsetNew == null) {
			return charOffsetOld;
		} else if (charOffsetOld == null) {
			return charOffsetNew;
		}

		return charOffsetOld > charOffsetNew ? charOffsetOld : charOffsetNew;
	}

	private CatEntity getCatEntity(String token, String name, Short band, Short score, Long charOffset, Long charCount) {
		Attribute attribute = new Attribute();
		attribute.setAttrSearchToken(token);
		attribute.setName(name);

		CatEntity catEntity = new CatEntity();
		catEntity.setAttribute(attribute);
		if (band > -1) {
			catEntity.setBand(band);
		}
		if (score > -1) {
			catEntity.setScore(score);
		}
		catEntity.setCharOffset(charOffset);
		catEntity.setCharCount(charCount);

		return catEntity;
	}

	private CatEntity getQualifyCatEntity(Group group, Map<String, CatEntity> matchedTaxonomyMap, Set<String> finallyMatchedEntities) {

		if (group == null || CollectionUtils.isEmpty(group.getTokenSet()) || MapUtils.isEmpty(matchedTaxonomyMap)) {
			return null;
		}

		boolean isAllEnttiesMatched = true;

		Short band = -1;
		Short score = -1;
		Long charOffset = null;
		Map<Long, Long> charMap = new HashMap<Long, Long>();

		Set<String> tokenSet = group.getTokenSet();
		for (String token : tokenSet) {

			if (!matchedTaxonomyMap.containsKey(token)) {
				isAllEnttiesMatched = false;
				break;
			}

			CatEntity catEntity = matchedTaxonomyMap.get(token);
			Long charOffsetRes = catEntity.getCharOffset();
			if (charOffsetRes != null) {
				charMap.put(charOffsetRes, catEntity.getCharCount());
			}

			LOG.debug("in response find min , token : " + token + ", charOffset : " + charOffsetRes + ", charCount : " + catEntity.getCharCount()
					+ ", band : " + catEntity.getBand() + ", score : " + catEntity.getScore());
			charOffset = getMinCharOffset(charOffset, charOffsetRes);
			band = getMinBand(band, catEntity.getBand() != null ? catEntity.getBand() : -1);
			score = getMinScore(score, catEntity.getScore() != null ? catEntity.getScore() : -1);

		}

		if (!isAllEnttiesMatched) {
			LOG.debug("All Entities not Matched for tokenSet : " + tokenSet);
			return null;
		}

		LOG.info("All Entities Matched for tokenSet : " + tokenSet + ", Finally to process for min -> FrToken : " + group.getFrToken()
				+ ", band : " + band + ", score : " + score + ", charOffset : " + charOffset + ", charCount : " + charMap.get(charOffset));

		finallyMatchedEntities.addAll(tokenSet);
		return getCatEntity(group.getFrToken(), group.getFrTopic(), band, score, charOffset, charMap.get(charOffset));
	}

	private Short getMinScore(Short score, Short score2) {
		if (score == -1) {
			return score2;
		}
		return (short) Math.min(score, score2);
	}

	private Short getMinBand(Short band, Short band2) {
		if (band == -1) {
			return band2;
		}
		return (short) Math.min(band, band2);
	}

	private Long getMinCharOffset(Long charOffsetOld, Long charOffsetNew) {

		if (charOffsetNew == null) {
			return charOffsetOld;
		} else if (charOffsetOld == null) {
			return charOffsetNew;
		}

		return charOffsetOld < charOffsetNew ? charOffsetOld : charOffsetNew;
	}

	private MultipleMatchedEntities populateMultipleMatchedEntitiesMap(List<CatEntity> catEntities, Map<String, Topic> topicMap,
			Set<String> pwTokens) {

		if (CollectionUtils.isEmpty(catEntities)) {
			return null;
		}

		LOG.info("Doc Categorize Response Size : " + catEntities.size());
		Set<String> frTokens = topicMap != null ? topicMap.keySet() : new HashSet<String>();
		Map<String, CatEntity> matchedTaxonomyMap = new HashMap<String, CatEntity>();
		Map<String, CatEntity> companyMap = new HashMap<String, CatEntity>();

		LOG.debug("frTokens in excel sheet : " + frTokens);
		for (CatEntity catEntity : catEntities) {

			String searchToken = catEntity.getAttribute().getAttrSearchToken();
			LOG.debug("searchToken from catEntity : " + searchToken);
			if (frTokens.contains(searchToken)) {
				matchedTaxonomyMap.put(searchToken, catEntity);
				Topic topic = topicMap.get(searchToken);
				if (topic != null) {
					Set<String> pwTokenSet = topic.getPwTokens();
					if (CollectionUtils.isNotEmpty(pwTokenSet)) {
						pwTokens.addAll(pwTokenSet);
					}
				}
			}

			if (StringUtils.isNotEmpty(searchToken) && searchToken.startsWith(SEPRATOR)) {
				companyMap.put(searchToken, catEntity);
			}
		}

		LOG.info("populate Multiple Matched Entities where Matched CompanyMap size : " + companyMap.size() + " and Matched TaxonomyMap size : "
				+ matchedTaxonomyMap.size() + " and pwTokenSet to process : " + pwTokens);
		MultipleMatchedEntities multipleMatchedEntities = new MultipleMatchedEntities();
		multipleMatchedEntities.setCompanyMap(companyMap);
		multipleMatchedEntities.setMatchedTaxonomyMap(matchedTaxonomyMap);
		return multipleMatchedEntities;

	}

	@SuppressWarnings("rawtypes")
	public JSONResponse getTakeDownResponse(int statusCode, String succMessageKey) {
		JSONResponse res = new JSONResponse();
		res.setVersion(version);
		res.setStatus(ResStatus.SUCCESS);
		String messageKey = null;
		if (statusCode == StatusCode.REQUEST_SUCCESS) {
			messageKey = succMessageKey;
		} else if (statusCode == StatusCode.ARTICLE_UNDER_PROCESSING) {
			messageKey = "takedown.status.processing";
		} else if (statusCode == StatusCode.ARTICLE_TAKED_DOWN) {
			messageKey = "errorcode." + statusCode;
		} else {
			messageKey = "errorcode." + statusCode;
			res.setStatus(ResStatus.ERROR);
		}

		res.setMessage(messageSource.getMessage(messageKey, null, Locale.getDefault()));
		return res;
	}
}
