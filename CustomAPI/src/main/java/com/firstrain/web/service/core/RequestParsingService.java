package com.firstrain.web.service.core;

import java.util.Locale;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.stereotype.Service;

import com.firstrain.frapi.util.StatusCode;
import com.firstrain.utils.JSONUtility;
import com.firstrain.web.response.CategorizationServiceResponse;
import com.firstrain.web.response.JSONResponse;
import com.firstrain.web.response.JSONResponse.ResStatus;
import com.firstrain.web.wrapper.EntityListWrapperData;

@Service
public class RequestParsingService {

	private static final Logger LOG = Logger.getLogger(RequestParsingService.class);

	@Autowired
	private ResourceBundleMessageSource messageSource;
	@Autowired
	private ServletContext servletContext;
	@Value("${api.version}")
	private String version;

	@PostConstruct
	private void init() {
		servletContext.setAttribute("version", version);
	}

	public String getRefinedReqVal(Object obj) {

		if (obj == null) {
			return null;
		}

		String val = obj.toString();
		if (val == null || (val = val.trim()).isEmpty()) {
			return null;
		}
		return val;
	}

	public String getRefinedReqVal(String val) {
		if (val == null || (val = val.trim()).isEmpty()) {
			return null;
		}
		return val;
	}

	public CategorizationServiceResponse getErrorResponseForCatgorizerService(int errorCode, String guid, String docId) {
		CategorizationServiceResponse res = new CategorizationServiceResponse();

		EntityListWrapperData resp = new EntityListWrapperData();
		res.setResult(resp);
		// set guid
		if (StringUtils.isNotEmpty(guid)) {
			resp.setGuid(guid);
		} else if (StringUtils.isNotEmpty(docId)) {
			resp.setDocId(docId);
		}

		res.setStatus(ResStatus.ERROR);
		res.setErrorCode(errorCode);
		String message = messageSource.getMessage(("errorcode." + errorCode), null, Locale.getDefault());
		res.setMessage(message);
		res.setVersion(version);
		return res;
	}

	@SuppressWarnings("rawtypes")
	public JSONResponse getErrorResponse(int errorCode) {
		if (errorCode < 0) {
			errorCode = StatusCode.INTERNAL_SERVER_ERROR;
		}
		JSONResponse res = new JSONResponse();
		String messageKey = "errorcode." + errorCode;
		String message = messageSource.getMessage(messageKey, null, Locale.getDefault());
		res.setStatus(ResStatus.ERROR);
		res.setMessage(message);
		res.setErrorCode(errorCode);
		res.setVersion(version);
		return res;
	}

	public String getSerializedMetadata(Map<String, Object> reqMap) {
		if (reqMap != null && reqMap.isEmpty()) {
			try {
				return JSONUtility.serialize(reqMap);
			} catch (Exception e) {
				LOG.error("Error in getSerializedMetadata : ", e);
			}
		}
		return null;
	}
}
