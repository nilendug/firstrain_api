package com.firstrain.web.service.core;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;

import org.apache.commons.httpclient.Header;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.RequestEntity;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.firstrain.utils.JSONUtility;

@Service
public class HttpClientService {

	private static final Logger LOG = Logger.getLogger(HttpClientService.class);

	@Qualifier("httpClient")
	@Autowired
	private HttpClient httpClient;

	public <T> T postDataInReqBody(String url, String method, Object reqBean, Class<T> responseType, Header... headers) throws Exception {
		PostMethod postMethod = null;
		String jsonInut = null;
		// long st = PerfMonitor.currentTime();
		String res = null;
		try {
			url = url + "/" + method;
			postMethod = new PostMethod(url);
			postMethod.addRequestHeader(new Header("Accept", "application/json"));
			if (headers != null) {
				for (Header hd : headers) {
					postMethod.addRequestHeader(hd);
				}
			}
			jsonInut = JSONUtility.serialize(reqBean);
			RequestEntity re = new StringRequestEntity(JSONUtility.serialize(reqBean), "application/json", "UTF-8");
			postMethod.setRequestEntity(re);
			httpClient.executeMethod(postMethod);
			InputStream in = postMethod.getResponseBodyAsStream();
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			byte[] buffer = new byte[1024 * 10];
			int n = 0;
			while ((n = in.read(buffer)) > -1) {
				out.write(buffer, 0, n);
			}
			res = new String(out.toByteArray());
			T t = JSONUtility.deserialize(res, responseType);
			LOG.info("Serving data from service POST: " + url + " :: reqBody: " + jsonInut);
			return t;
		} catch (Exception e) {
			LOG.error("Error while getting data for: " + url + " :: reqBody: " + jsonInut + " :: response: " + res, e);
			throw e;
		} finally {
			if (postMethod != null) {
				postMethod.releaseConnection();
			}
			// PerfMonitor.recordStats(st, PerfActivityType.Rest, method);
		}
	}

}
